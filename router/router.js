const express = require("express");
const router = express.Router();


router.route("/home").get((req, res) => {
    res.render("homepage");
})

router.route("/playnow").get((req, res) => {
    res.render("playpage");
})

module.exports = router;
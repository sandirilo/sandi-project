function ambilPilihanKompi() {
    const com = Math.random();
    if(com < 0.34) {
        return "batu" ;
    }
    if(com >= 0.34 && com < 0.67) {
        return "kertas";
    }
    return "gunting";
}



function getResult (player, com) {
    if (player === com) {
        return "DRAW";
    } else if (player == "batu") {
        if (com == "kertas") {
            return "COM WIN";
        } else if (com == "gunting"){
            return "PLAYER WIN";
        }
    } else if (player == "kertas") {
        if (com === "batu") {
            return "PLAYER WIN";
        } else if (com === "gunting"){
            return "COM WIN";
        }
    } else if (player == "gunting") {
        if (com == "batu") {
            return "COM WIN";
        } else if (com === "kertas"){
            return "PLAYER WIN";
        }
    }
}


let batuPlayer = document.querySelector(".batuplayer");
batuPlayer.addEventListener("click", function() {
    ambilPilihanKompi();
    const pilihanPlayer = "batu";
    const pilihanComputer = ambilPilihanKompi();
    const hasil = getResult(pilihanPlayer, pilihanComputer)
    console.log(pilihanPlayer);
    console.log(pilihanComputer);
    console.log(hasil);
    const show = document.getElementById("h1");
    show.innerHTML = hasil;
    console.log(show);
})

let kertasPlayer = document.querySelector(".kertasplayer");
kertasPlayer.addEventListener("click", function() {
    ambilPilihanKompi();
    const pilihanPlayer = "kertas";
    const pilihanComputer = ambilPilihanKompi();
    const hasil = getResult(pilihanPlayer, pilihanComputer)
    console.log(pilihanPlayer);
    console.log(pilihanComputer);
    console.log(hasil);
    const show = document.getElementById("h1");
    show.innerHTML = hasil;
    console.log(show);
})

let guntingPlayer = document.querySelector(".guntingplayer");
guntingPlayer.addEventListener("click", function() {
    ambilPilihanKompi();
    const pilihanPlayer = "gunting";
    const pilihanComputer = ambilPilihanKompi();
    const hasil = getResult(pilihanPlayer, pilihanComputer)
    console.log(pilihanPlayer);
    console.log(pilihanComputer);
    console.log(hasil);
    const show = document.getElementById("h1");
    show.innerHTML = hasil;
    console.log(show);
})

let refresh = document.querySelector(".refresh");
refresh.addEventListener("click", function() {
    history.go(0);   
})

let kembali = document.querySelector(".kembali");
kembali.addEventListener("click", function() {
    history.back()
})
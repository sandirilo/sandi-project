const express = require("express");
const app = express();
const port = 3000;
const router = require("./router/router");

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static(__dirname + "/views"));
app.set("view engine", "ejs");
app.use(router);
app.use(express.static(__dirname + "/style"))




app.listen(port, ()=> {
    console.log(`server running on port ${port}`);
})